export default () => ({
  port: parseInt(process.env.PORT, 10) || 8080,
  sessionSecret: '3FBD8C28D2183FC45BEFB676266EC',
  sessionsPath: process.env.SESSIONS_PATH || './sessions',
  dbPath: process.env.DB_PATH || './db.sqlite3',
  storePath: process.env.STORE_PATH || './store'
});
