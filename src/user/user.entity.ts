import {
  Entity,
  Column,
  Unique,
  PrimaryGeneratedColumn,
  OneToMany
} from 'typeorm';
import { Topic } from 'src/topic/topic.entity';
import { Post } from 'src/post/post.entity';

@Entity({
  name: 'users'
})
@Unique(['username'])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column({ select: false })
  password: string;

  @Column()
  credit: number;

  @OneToMany(
    type => Topic,
    topic => topic.user
  )
  topics: Topic[];

  @OneToMany(
    type => Post,
    post => post.user
  )
  posts: Post[];
}
