import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) {}

  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async findOne(username: string): Promise<User> {
    return await this.userRepository.findOne({ username });
  }

  async findOneWithPassword(username: string): Promise<User> {
    return await this.userRepository.findOne({
      where: { username },
      select: ['id', 'password', 'username']
    });
  }

  async create(dto: CreateUserDto): Promise<User> {
    const { password, username } = dto;
    const [_, count] = await this.userRepository.findAndCount({ username });
    if (count) {
      throw new BadRequestException('user already exist');
    }
    let user = new User();
    user.password = password;
    user.username = username;
    await this.userRepository.save(user);
    return user;
  }
}
