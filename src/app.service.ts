import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "./user/user.entity";

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) {}

  getHello(): string {
    return "Hello World!";
  }

  async getUser(id: number) {
    return await this.userRepository.findOne({ id: id });
  }
}
