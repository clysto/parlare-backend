import {
  Controller,
  Get,
  Req,
  Res,
  Post,
  UseGuards,
  Body
} from '@nestjs/common';
import { AppService } from './app.service';
import { CreateUserDto } from './user/dto/create-user.dto';
import { Request, Response } from 'express';
import { LoginGuard } from './common/guards/login.guard';
import { AuthenticatedGuard } from './common/guards/authenticated.guard';
import { ReqUser } from './common/decorators/req-user.decorator';
import { User } from './user/user.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @UseGuards(LoginGuard)
  @Post('login')
  async login(@Req() req: Request, @Body() dto: CreateUserDto) {
    return req.user;
  }

  @UseGuards(AuthenticatedGuard)
  @Get('profile')
  getProfile(@ReqUser() user: User) {
    return this.appService.getUser(user.id);
  }

  @Get('user')
  getUser(@Req() req: Request) {
    return req.user;
  }

  @Get('logout')
  logout(@Req() req: Request, @Res() res: Response) {
    req.logout();
    res.redirect('/');
  }
}
