import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Topic } from './topic.entity';
import { CreateTopicDto } from './dto/create-topic.dto';
import { User } from 'src/user/user.entity';
import { PaginationData } from 'src/common/interfaces/pagination-data.interface';
import { Page } from 'src/common/interfaces/page.interface';

@Injectable()
export class TopicService {
  constructor(
    @InjectRepository(Topic)
    private readonly topicRepository: Repository<Topic>
  ) {}

  async findAll(
    paginationData?: PaginationData
  ): Promise<Topic[] | Page<Topic>> {
    if (paginationData) {
      const { page, size } = paginationData;
      const [content, count] = await this.topicRepository
        .createQueryBuilder('topic')
        .leftJoinAndSelect('topic.user', 'user')
        .loadRelationCountAndMap('topic.postsCount', 'topic.posts')
        .skip(page * size)
        .take(size)
        .orderBy('topic.time', 'DESC')
        .getManyAndCount();
      const totalPages = Math.ceil(count / size);
      return {
        content,
        first: page === 0,
        last: page === totalPages - 1,
        totalPages,
      };
    } else {
      return await this.topicRepository.find();
    }
  }

  async findOne(id: number): Promise<Topic> {
    return this.topicRepository
      .createQueryBuilder('topic')
      .where({ id })
      .leftJoinAndSelect('topic.user', 'user')
      .loadRelationCountAndMap('topic.postsCount', 'topic.posts')
      .getOne();
  }

  async findAllByUser(
    user: User,
    paginationData: PaginationData
  ): Promise<Page<Topic>> {
    const { page, size } = paginationData;
    const [content, count] = await this.topicRepository
      .createQueryBuilder('topic')
      .where({ user })
      .leftJoinAndSelect('topic.user', 'user')
      .loadRelationCountAndMap('topic.postsCount', 'topic.posts')
      .skip(page * size)
      .take(size)
      .orderBy('topic.time', 'DESC')
      .getManyAndCount();
    const totalPages = Math.ceil(count / size);
    return {
      content,
      first: page === 0,
      last: page === totalPages - 1,
      totalPages,
    };
  }

  async create(dto: CreateTopicDto, user: User): Promise<Topic> {
    const { title, content } = dto;
    let topic = new Topic();
    topic.title = title;
    topic.content = content;
    topic.user = user;
    await this.topicRepository.save(topic);
    return topic;
  }

  async remove(user: User, id: number) {
    const topic = await this.topicRepository.findOne({ user, id });
    if (topic) {
      await this.topicRepository.remove(topic);
    }
  }
}
