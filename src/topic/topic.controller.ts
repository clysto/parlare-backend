import {
  Controller,
  Get,
  Post,
  Param,
  Body,
  Req,
  UseGuards,
  Delete
} from '@nestjs/common';
import { TopicService } from './topic.service';
import { Topic } from './topic.entity';
import { CreateTopicDto } from './dto/create-topic.dto';
import { Request } from 'express';
import { AuthenticatedGuard } from 'src/common/guards/authenticated.guard';
import { User } from 'src/user/user.entity';
import { Pagination } from 'src/common/decorators/pagination.decorator';
import { PaginationData } from 'src/common/interfaces/pagination-data.interface';
import { Page } from 'src/common/interfaces/page.interface';
import { ReqUser } from 'src/common/decorators/req-user.decorator';

@Controller('topics')
export class TopicController {
  constructor(private readonly topicService: TopicService) {}

  @Get()
  async findAll(
    @Pagination() pagination: PaginationData
  ): Promise<Topic[] | Page<Topic>> {
    return this.topicService.findAll(pagination);
  }

  @Get('user')
  @UseGuards(AuthenticatedGuard)
  async findAllByUser(
    @ReqUser() user: User,
    @Pagination() pagination: PaginationData
  ): Promise<Page<Topic>> {
    return this.topicService.findAllByUser(user, pagination);
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Topic> {
    return await this.topicService.findOne(id);
  }

  @Post()
  @UseGuards(AuthenticatedGuard)
  async create(
    @Req() req: Request,
    @Body() dto: CreateTopicDto
  ): Promise<Topic> {
    return await this.topicService.create(dto, req.user as User);
  }

  @Delete(':id')
  @UseGuards(AuthenticatedGuard)
  async remove(@Req() req: Request, @Param('id') id: number) {
    this.topicService.remove(req.user as User, id);
  }
}
