import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
  CreateDateColumn
} from 'typeorm';
import { User } from 'src/user/user.entity';
import { Post } from 'src/post/post.entity';

@Entity({
  name: 'topics'
})
export class Topic {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column('text')
  content: string;

  @CreateDateColumn()
  time: Date;

  @ManyToOne(
    type => User,
    user => user.topics
  )
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToMany(
    type => Post,
    post => post.topic
  )
  posts: Post[];

  postsCount?: number;
}
