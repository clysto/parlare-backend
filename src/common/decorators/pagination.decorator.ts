import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';
import { PaginationData } from '../interfaces/pagination-data.interface';

export const Pagination = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): PaginationData => {
    const request = ctx.switchToHttp().getRequest() as Request;
    const { page, size } = request.query;
    return {
      page: parseInt(page as string) || 0,
      size: parseInt(size as string) || 10,
    };
  }
);
