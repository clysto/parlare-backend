export interface PaginationData {
  page: number;
  size: number;
}
