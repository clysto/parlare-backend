import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import * as cookie from 'cookie';
import { SessionService } from 'src/session/session.service';

@Injectable()
export class WsAuthGuard implements CanActivate {
  constructor(private readonly sessionService: SessionService) {}

  canActivate(context: ExecutionContext): boolean {
    const cookies = context.switchToWs().getClient().handshake.headers.cookie;
    console.log(cookie.parse(cookies));
    const sessionStore = this.sessionService.getSessionStore();
    sessionStore.get(
      cookie.parse(cookies)['connect.sid'].split(':')[1].split('.')[0],
      (err, session) => {
        console.log(session);
      }
    );
    return true;
  }
}
