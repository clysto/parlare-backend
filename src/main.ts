import { NestFactory } from "@nestjs/core";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./app.module";
import * as session from "express-session";
import * as passport from "passport";
import { ValidationPipe } from "@nestjs/common";
import * as express from "express";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle("Parlare API")
    .setDescription("Parlare API documentation.")
    .setVersion("0.0.1")
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("api", app, document);

  app.use(
    session({
      secret: app.get("ConfigService").get("sessionSecret"),
      resave: false,
      store: app.get("SessionService").getSessionStore(),
      saveUninitialized: false,
      cookie: {
        maxAge: 7 * 24 * 60 * 60 * 1000,
      },
    })
  );

  app.use(passport.initialize());
  app.use(passport.session());

  const storePath = app.get("ConfigService").get("storePath");
  app.use("/files", express.static(storePath));

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    })
  );

  await app.listen(app.get("ConfigService").get("port"));
}
bootstrap();
