import {
  Controller,
  Get,
  Param,
  Res,
  Post,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from "@nestjs/common";
import { AuthenticatedGuard } from "src/common/guards/authenticated.guard";
import { FileInterceptor } from "@nestjs/platform-express";
import { ResourceService } from "./resource.service";
import { ReqUser } from "src/common/decorators/req-user.decorator";
import { User } from "src/user/user.entity";

@Controller("resource")
export class ResourceController {
  constructor(private readonly resourceService: ResourceService) {}

  @Get("")
  async listFiles() {
    return this.resourceService.getAllFiles();
  }

  @Get(":uuid/:filename")
  async download(
    @Param("uuid") uuid: string,
    @Param("filename") filename: string,
    @Res() response
  ) {
    return (await this.resourceService.getFile(uuid, filename)).pipe(response);
  }

  @Post("")
  @UseGuards(AuthenticatedGuard)
  @UseInterceptors(FileInterceptor("file"))
  async upload(@UploadedFile() file, @ReqUser() user: User) {
    return await this.resourceService.createFile(file, user);
  }

  @Delete(":id")
  @UseGuards(AuthenticatedGuard)
  async remove(@Param("id") id: number, @ReqUser() user: User) {
    await this.resourceService.removeFile(id, user);
  }
}
