import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
} from "typeorm";
import { User } from "src/user/user.entity";

@Entity({
  name: "resource",
})
export class Resource {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne((type) => User)
  @JoinColumn({ name: "user_id" })
  user: User;

  @CreateDateColumn()
  time: Date = new Date();

  @Column()
  uuid: string;
}
