import { Module } from "@nestjs/common";
import { ResourceController } from "./resource.controller";
import { ResourceService } from "./resource.service";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { Resource } from "./resource.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MulterModule } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import { v4 as uuidv4 } from "uuid";
import * as path from "path";
import * as fs from "fs";

@Module({
  imports: [
    TypeOrmModule.forFeature([Resource]),
    MulterModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          storage: diskStorage({
            destination: async (req, file, cb) => {
              const storePath: string = configService.get("storePath");
              file.uuid = uuidv4();
              const filePath = path.resolve(storePath, file.uuid);
              if (!fs.existsSync(filePath)) {
                fs.mkdirSync(filePath);
              }
              return cb(null, filePath);
            },
            filename: (req, file, cb) => {
              return cb(null, file.originalname);
            },
          }),
        };
      },
    }),
  ],
  controllers: [ResourceController],
  providers: [ResourceService, ConfigService],
})
export class ResourceModule {}
