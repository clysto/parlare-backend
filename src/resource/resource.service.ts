import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/user/user.entity";
import { ConfigService } from "@nestjs/config";
import { Resource } from "src/resource/resource.entity";
import { Repository } from "typeorm";
import * as fs from "fs";
import * as path from "path";

@Injectable()
export class ResourceService {
  storePath: string;

  constructor(
    private readonly configService: ConfigService,
    @InjectRepository(Resource)
    private readonly resourceRepository: Repository<Resource>
  ) {
    this.storePath = this.configService.get("storePath");
  }

  async removeFile(id: number, user: User) {
    const resource = await this.resourceRepository.findOne({ id, user });
    if (resource) {
      await this.resourceRepository.remove(resource);
      fs.unlinkSync(path.resolve(this.storePath, resource.uuid, resource.name));
      fs.rmdirSync(path.resolve(this.storePath, resource.uuid));
    }
  }

  async createFile(file: any, user: User) {
    let resource = new Resource();
    resource.uuid = file.uuid;
    resource.name = file.originalname;
    resource.user = user;
    await this.resourceRepository.save(resource);
    return resource;
  }

  async getAllFiles() {
    return await this.resourceRepository.find({
      relations: ["user"],
      order: { time: "DESC" },
    });
  }

  async getFile(uuid: string, filename: string) {
    return fs.createReadStream(path.resolve(this.storePath, uuid, filename));
  }
}
