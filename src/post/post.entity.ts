import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
  AfterLoad,
  CreateDateColumn
} from 'typeorm';
import { User } from 'src/user/user.entity';
import { Topic } from 'src/topic/topic.entity';
import { Like } from './like.entity';

@Entity({
  name: 'posts'
})
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  content: string;

  @Column({ name: 'topic_id' })
  topicId: number;

  @CreateDateColumn()
  time: Date = new Date();

  @ManyToOne(
    type => Topic,
    topic => topic.posts
  )
  @JoinColumn({ name: 'topic_id' })
  topic: Topic;

  @ManyToOne(
    type => User,
    user => user.posts
  )
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToMany(
    type => Like,
    like => like.post
  )
  likes: Like[];

  likesCount?: number;

  liked: any;

  @AfterLoad()
  updateCounters() {
    if (this.liked) {
      this.liked = true;
    } else {
      this.liked = false;
    }
  }
}
