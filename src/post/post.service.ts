import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { Post } from "./post.entity";
import { User } from "src/user/user.entity";
import { CreatePostDto } from "./dto/create-post.dto";
import { Page } from "src/common/interfaces/page.interface";
import { PaginationData } from "src/common/interfaces/pagination-data.interface";
import { Like } from "./like.entity";

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private readonly postRepository: Repository<Post>,
    @InjectRepository(Like)
    private readonly likeRepository: Repository<Like>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) {}

  async findAllByTopicId(
    topicId: number,
    paginationData: PaginationData,
    user?: User
  ): Promise<Page<Post>> {
    const { page, size } = paginationData;
    const [content, count] = await this.postRepository
      .createQueryBuilder("post")
      .where("post.topicId = :topicId", { topicId })
      .leftJoinAndMapOne(
        "post.liked",
        "post.likes",
        "like",
        "like.userId = :userId",
        { userId: user?.id ?? -1 }
      )
      .leftJoinAndSelect("post.user", "user")
      .loadRelationCountAndMap("post.likesCount", "post.likes")
      .skip(page * size)
      .take(size)
      .orderBy("post.time", "ASC")
      .getManyAndCount();

    const totalPages = Math.ceil(count / size);
    return {
      content,
      first: page === 0,
      last: page === totalPages - 1,
      totalPages,
    };
  }

  async create(dto: CreatePostDto, user: User, topicId: number): Promise<Post> {
    user = await this.userRepository.findOne({ id: user.id });
    const { content } = dto;
    let post = new Post();
    post.content = content;
    post.user = user;
    post.topicId = topicId;
    await this.postRepository.save(post);
    user.credit += 5;
    post.likesCount = 0;
    post.liked = false;
    await this.userRepository.save(user);
    return post;
  }

  async remove(user: User, id: number) {
    const post = await this.postRepository.findOne({ user, id });
    if (post) {
      await this.postRepository.remove(post);
    }
  }

  async increaseLikes(user: User, id: number) {
    const like = await this.findLike(user, id);
    const post = await this.postRepository.findOne(id);
    // 检查是否已经点过赞
    if (post && !like) {
      let like = new Like();
      like.post = post;
      like.user = user;
      await this.likeRepository.save(like);
    }
  }

  async decreaseLikes(user: User, id: number) {
    const like = await this.findLike(user, id);
    if (like) {
      await this.likeRepository.remove(like);
    }
  }

  private async findLike(user: User, id: number): Promise<Like> {
    return await this.likeRepository.findOne({
      user: user,
      postId: id,
    });
  }
}
