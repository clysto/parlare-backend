import {
  Controller,
  Get,
  Param,
  Body,
  Req,
  Post,
  Delete,
  UseGuards
} from '@nestjs/common';
import { Post as PostEntity } from './post.entity';
import { PostService } from './post.service';
import { CreatePostDto } from './dto/create-post.dto';
import { Request } from 'express';
import { User } from 'src/user/user.entity';
import { AuthenticatedGuard } from 'src/common/guards/authenticated.guard';
import { Page } from 'src/common/interfaces/page.interface';
import { Pagination } from 'src/common/decorators/pagination.decorator';
import { PaginationData } from 'src/common/interfaces/pagination-data.interface';

@Controller('posts')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Get(':topicId')
  async findAllByTopicId(
    @Req() req: Request,
    @Pagination() pagination: PaginationData,
    @Param('topicId') topicId: number
  ): Promise<PostEntity[] | Page<PostEntity>> {
    return this.postService.findAllByTopicId(
      topicId,
      pagination,
      req.user as User
    );
  }

  @Post(':topicId')
  async create(
    @Req() req: Request,
    @Param('topicId') topicId: number,
    @Body() dto: CreatePostDto
  ): Promise<PostEntity> {
    return await this.postService.create(dto, req.user as User, topicId);
  }

  @Delete(':id')
  @UseGuards(AuthenticatedGuard)
  async remove(@Req() req: Request, @Param('id') id: number) {
    this.postService.remove(req.user as User, id);
  }

  @Post(':id/like')
  @UseGuards(AuthenticatedGuard)
  async createLike(@Req() req: Request, @Param('id') id: number) {
    this.postService.increaseLikes(req.user as User, id);
  }

  @Delete(':id/like')
  @UseGuards(AuthenticatedGuard)
  async removeLike(@Req() req: Request, @Param('id') id: number) {
    this.postService.decreaseLikes(req.user as User, id);
  }
}
