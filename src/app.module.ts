import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserModule } from "./user/user.module";
import { AuthModule } from "./auth/auth.module";
import { TopicModule } from "./topic/topic.module";
import { PostModule } from "./post/post.module";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { SessionModule } from "./session/session.module";
import { ResourceModule } from "./resource/resource.module";
import { TaskModule } from "./task/task.module";
import configuration from "./config/configuration";
import { User } from "./user/user.entity";

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: "sqlite",
        database: configService.get<string>("dbPath"),
        autoLoadEntities: true,
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([User]),
    UserModule,
    AuthModule,
    TopicModule,
    PostModule,
    ConfigModule.forRoot({ load: [configuration] }),
    SessionModule,
    ResourceModule,
    TaskModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
