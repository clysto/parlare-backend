import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
} from "typeorm";
import { User } from "src/user/user.entity";

@Entity({
  name: "tasks",
})
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  content: string;

  @ManyToOne((type) => User)
  @JoinColumn({ name: "user_id" })
  user: User;

  @CreateDateColumn()
  time: Date = new Date();
}
