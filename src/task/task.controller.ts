import {
  Controller,
  Get,
  Post,
  Delete,
  UseGuards,
  Body,
  Param,
} from "@nestjs/common";
import { TaskService } from "./task.service";
import { AuthenticatedGuard } from "src/common/guards/authenticated.guard";
import { ReqUser } from "src/common/decorators/req-user.decorator";
import { User } from "src/user/user.entity";
import { CreateTaskDto } from "./dto/create-task.dto";

@Controller("tasks")
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get("/")
  async getAllTasks() {
    return this.taskService.getAllTasks();
  }

  @UseGuards(AuthenticatedGuard)
  @Post("/")
  async createTask(@ReqUser() user: User, @Body() dto: CreateTaskDto) {
    return await this.taskService.createTask(user, dto);
  }

  @Delete("/:id")
  @UseGuards(AuthenticatedGuard)
  async deleteTask(@ReqUser() user: User, @Param("id") id: number) {
    await this.taskService.deleteTask(id, user);
  }
}
