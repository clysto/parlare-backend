import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Task } from "./task.entity";
import { CreateTaskDto } from "./dto/create-task.dto";
import { User } from "src/user/user.entity";

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>
  ) {}

  async getAllTasks() {
    return this.taskRepository.find({
      relations: ["user"],
      order: {
        time: "DESC",
      },
    });
  }

  async createTask(user: User, dto: CreateTaskDto) {
    let task = new Task();
    task.content = dto.content;
    task.user = user;
    return await this.taskRepository.save(task);
  }

  async deleteTask(id: number, user: User) {
    const task = await this.taskRepository.findOne({ id, user });
    if (task) {
      await this.taskRepository.remove(task);
    }
  }
}
