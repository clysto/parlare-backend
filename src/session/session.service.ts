import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as session from 'express-session';
import * as FileStoreBuilder from 'session-file-store';

const FileStore = FileStoreBuilder(session);

@Injectable()
export class SessionService {
  private store: session.Store;

  constructor(private readonly configService: ConfigService) {
    this.store = new FileStore({
      path: this.configService.get('sessionsPath'),
    });
  }

  getSessionStore() {
    return this.store;
  }
}
