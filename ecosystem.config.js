module.exports = {
  apps: [
    {
      name: 'parlare-backend',
      script: 'npm',
      args: 'run start:prod',
      env: {
        NODE_ENV: 'development',
        DB_PATH: './db.sqlite3',
        SESSIONS_PATH: './sessions',
        STORE_PATH: './store'
      },
      env_production: {
        NODE_ENV: 'production',
        PORT: 3006,
        DB_PATH: '/srv/parlare-backend/db.sqlite3',
        SESSIONS_PATH: '/srv/parlare-backend/sessions',
        STORE_PATH: '/srv/parlare-backend/store'
      },
    },
  ],
  deploy: {
    production: {
      user: 'root',
      host: 'maoyachen.com',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:clysto/parlare-backend.git',
      path: '/var/app/parlare-backend',
      'post-deploy':
        'npm install && npm run build && pm2 startOrRestart ecosystem.config.js --env production',
    },
  },
};
